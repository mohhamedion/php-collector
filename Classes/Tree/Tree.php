<?php

namespace Classes\Tree;

use Classes\Fruits\AbstractFruitClass;

class Tree
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public $fruits = [];

    public function addFruit(AbstractFruitClass $fruits)
    {
        $this->fruits[] = $fruits;
    }

    public function getFruits(): array
    {
        return $this->fruits;
    }
}