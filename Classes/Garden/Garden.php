<?php

namespace Classes\Garden;

use Classes\Tree\Tree;

class Garden
{
    public $trees;

    public function __construct()
    {
    }

    public function addTree(Tree $tree)
    {
        $this->trees[] = $tree;
    }

    public function getTrees()
    {
        return $this->trees;
    }

}
