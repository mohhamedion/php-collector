<?php

namespace Classes\Fruits;

class Pear extends AbstractFruitClass
{
    const name = 'Груша';

    public function generateWeight(): int
    {
        return rand(130, 170);
    }
}