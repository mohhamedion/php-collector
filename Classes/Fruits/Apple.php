<?php

namespace Classes\Fruits;

class Apple extends AbstractFruitClass
{
    const name = 'Яблоко';

    public function generateWeight(): int
    {
        return rand(150, 180);
    }
}