<?php

namespace Classes\Fruits;

abstract class AbstractFruitClass
{

    public $weight;

    public function __construct()
    {
        $this->weight = $this->generateWeight();
    }

    abstract function generateWeight(): int;

    public function getWeight(): int
    {
        return $this->weight;
    }

}