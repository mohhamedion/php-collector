<?php

namespace Classes\Collector;

use Classes\Fruits\AbstractFruitClass;
use Classes\Tree\Tree;

class Collector
{
    /**
     * @var
     */
    public $container = [];

    /**
     * @param Tree $tree
     * Собирать плоды с дерева
     * @return void
     */
    public function harvestFruit(Tree $tree)
    {
        foreach ($tree->getFruits() as $fruit) {
            $this->container[get_class($fruit)][] = $fruit;
        }
    }

    /**
     * @return array
     * Для получения всех видов фруктов, собранных в сборщике
     */
    public function getAvailableFruitTypesInContainers(): array
    {
        return array_keys($this->container);
    }

    /**
     * @param $fruitCLass
     * @return AbstractFruitClass[]
     */
    public function getContainer($fruitCLass): array
    {
        return $this->container[$fruitCLass];
    }

    public function countFruitInContainer($fruitClass): int
    {
        return count($this->getContainer($fruitClass));
    }

    public function countContainerWeight($fruitClass): int
    {
        $fruitCount = 0;
        foreach ($this->getContainer($fruitClass) as $fruit) {
            $fruitCount += $fruit->getWeight();
        }
        return $fruitCount / 1000;
    }
}