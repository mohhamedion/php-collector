<?php
function autoloader($class)
{
    require str_replace('\\', '/', $class) . '.php';
}
spl_autoload_register('autoloader');


use Classes\Fruits\AbstractFruitClass;
use Classes\Fruits\Apple;
use Classes\Tree\Tree;
use Classes\Fruits\Pear;
use Classes\Garden\Garden;
use Classes\Collector\Collector;


// создать сад
$garden = new Garden();

// Добавлять деревья в сад
for ($j = 0; $j < 10; $j++) {
    $appleTree = new Tree($j . '_apple');
    for ($i = 0; $i < rand(40, 50); $i++) {
        $appleTree->addFruit(new Apple());
    }
    $garden->addTree($appleTree);
}


for ($j = 0; $j < 15; $j++) {
    $pearTree = new Tree($j . '_pear');
    for ($i = 0; $i < rand(0, 20); $i++) {
        $pearTree->addFruit(new Pear());
    }
    $garden->addTree($pearTree);
}


// сборщик
$collector = new Collector();

//Собирать плоды со всех деревьев, добавленных в сад
foreach ($garden->getTrees() as $tree){
    $collector->harvestFruit($tree);
}

foreach ($collector->getAvailableFruitTypesInContainers() as $type)
{
    /**
     * @var AbstractFruitClass $type
     */
    echo "Общий вес собранных фруктов вида (".$type::name.") \n";
    echo $collector->countContainerWeight($type) ." Кг\n";
    echo "Кол-во собранных фруктов вида (".$type::name.") \n";
    echo $collector->countFruitInContainer($type)."\n";
    echo "________\n";
}


